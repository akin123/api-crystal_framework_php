<?php

/**
* @Author Deepak
* @Class Parameters Validate Class
* @Date 23/01/2016  
*/
class Parameters extends Service{

	public static function required($vars){
		$postedVars = Service::getVars();
		$postedVarsKeys = array_keys(Service::getVars());
		//$diff = array_merge(array_diff($vars,$postedVars),array_diff($postedVars,$vars));
		$diff = array_diff($vars,$postedVarsKeys);

		if(empty($diff)){

			$requiredDiff = array_diff($postedVarsKeys,$vars);

			foreach ($requiredDiff as $var) {
				unset($postedVars[$var]);
			}

			// Empty values passed in request for a required parameter
			$requestVars = array_map('trim', $postedVars);

			if(in_array("", $requestVars)){
				$result = array_filter( $requestVars, 'strlen' );
				$requestVars = array_keys($result);
				$diff = array_merge(array_diff($vars,$requestVars),array_diff($requestVars,$vars));

				$message = "Null value not allowed for ".implode(', ', $diff)." parameter(s)";
				header('HTTP/1.0 400 Bad Request');
				header('Content-Type: application/json');
				$response = array(
					'status' => 'error',
					'message'=> '400 Bad Request - '.$message
					);
				echo json_encode($response);
				exit;
			}
			return true;
		}else{
			$message = "Missing ".implode(', ', $diff)." Parameter(s)";
			header('HTTP/1.0 400 Bad Request');
			header('Content-Type: application/json');
			$response = array(
				'status' => 'error',
				'message'=> '400 Bad Request - '.$message
				);
			echo json_encode($response);
			exit;
		}
	}

}