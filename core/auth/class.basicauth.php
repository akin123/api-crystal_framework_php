<?php

class BasicAuth{
	private static $token;
	
	public static function getHeaders(){

		$headerStack = getallheaders(); // Requires php 5.4+ 

		if(isset($headerStack['Authorization'])){
			BasicAuth::$token = end(explode(' ', $headerStack['Authorization']));
		}

		if(in_array(BasicAuth::$token, TokenServer::getKeys())){
			return true;
		}
		else{
			return false;
		}

	}

	public static function enable(){

		if(!BasicAuth::getHeaders()){

			header('HTTP/1.0 401 Unauthorized');
			header('Content-Type: application/json');
			$response = array(
				'status' => 'error',
				'message'=> '401 Unauthorized'
				);
			echo json_encode($response);
			exit;
		}
	}
}




