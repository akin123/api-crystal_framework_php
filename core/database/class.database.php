<?php

/**
* @Author Deepak
* @Class MySql Database Abstraction
* @Date 21/01/2016  
*/
class Database implements DatabaseDriver{

	private static $connection;
	private static $statement;
	private static $result;
	private static $lastId;

	public static function connect(){
		
		try {
			Database::$connection = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST, DB_USER, DB_PASS);
			Database::$connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			Database::$connection->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
		} catch (PDOException $e) {
			header('HTTP/1.0 200 OK');
			header('Content-Type: application/json');
			$response = array(
				'status' => 'error',
				'message'=> 'MySql Error: '.$e->getMessage(),
				);
			echo json_encode($response);
			exit;
		}
	}

	public static function query($query){
		Database::connect();

        // prepare statement
		try{
			Database::$statement = Database::$connection->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		}catch (PDOException $e){
			header('HTTP/1.0 200 OK');
			header('Content-Type: application/json');
			$response = array(
				'status' => 'error',
				'message'=> 'MySql Error: '.$e->getMessage(),
				);
			echo json_encode($response);
			exit;
		}
	}

	public static function execute($variables = NULL, $assoc = false){

		try{
			Database::$statement->execute($variables);
		}catch (PDOException $e){
			header('HTTP/1.0 200 OK');
			header('Content-Type: application/json');
			$response = array(
				'status' => 'error',
				'message'=> 'MySql Error: '.$e->getMessage(),
				);
			echo json_encode($response);
			exit;
		}		

		try{
			Database::$result = Database::$statement->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e){
			//print_r($e->getMessage());
		}

		if(is_array(Database::$result)){

        // If only one result row
			if(!$assoc){
				if(count(Database::$result) == 1){
					Database::$result = end(Database::$result);
				}
			}

		}else{
			Database::$result = array();
		}

		// To get last inserted id
		Database::$lastId = Database::$connection->lastInsertId();

		return Database::$result; 
	}

	public static function result(){
		return Database::$result;
	}

	public static function lastId(){
		return Database::$lastId;
	}

}