<?php

require_once 'core/router/autoload.php';
require_once 'core/router/dispatcher.php';
require_once 'core/service/class.service.php';
require_once 'core/service/class.rest.php';
require_once 'core/http/class.http.php';
require_once 'core/database/class.databasedriver.php';
require_once 'core/database/class.database.php';
require_once 'core/auth/class.tokenserver.php';
require_once 'core/auth/class.basicauth.php';
require_once 'core/parameters/class.parameters.php';
require_once 'core/response/class.response.php';

require_once(dirname(__FILE__) . '/config/database.php');

class Bootstrap{

    public static function initialize(){
        
        // Enable Basic Authorization
        BasicAuth::enable();

        $dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $router) {

        /**********************************************************
         All routes must be defined here [method][uri][service]
        ************************ START ***************************/

         //$router->addRoute('POST', '/api/login','loginService');


        /************************* END *************************/
        }); 

        Router::dispatch($dispatcher);
    }
}

// Initialize routes
Bootstrap::initialize();